package it.uniba.tennisgame;

import static org.junit.Assert.*;

import org.junit.Test;

public class PlayerTest {
	
	//equivalence class testing
	//method incrementScore()
	@Test
	public void scoreShouldBeIncreased() {
		//Arrange
		Player player = new Player("Federer", 0);
		//Act
		player.incrementScore();
		//Assert
		assertEquals(1, player.getScore());
	}

	//class not increased
	@Test
	public void scoreShouldNotBeIncreased() {
		//Arrange
		Player player = new Player("Federer", 0);
		//Act
		
		//Assert
		assertEquals(0, player.getScore());
	}
	
	//method getScoreAsString()
	//class 0
	@Test
	public void scoreShouldBeLove() {
		//Arrange
		Player player = new Player("Federer", 0);
		//Act
		String scoreAsString = player.getScoreAsString();
		//Assert
		assertEquals("love", scoreAsString);
	}
	
	//class 1
	@Test
	public void scoreShouldBeFifteen() {
		//Arrange
		Player player = new Player("Federer", 1);
		//Act
		String scoreAsString = player.getScoreAsString();
		//Assert
		assertEquals("fifteen", scoreAsString);
	}
	
	//class 2
	@Test
	public void scoreShouldBeThirty() {
		//Arrange
		Player player = new Player("Federer", 2);
		//Act
		String scoreAsString = player.getScoreAsString();
		//Assert
		assertEquals("thirty", scoreAsString);
	}
	
	//class 3
	@Test
	public void scoreShouldBeForty() {
		//Arrange
		Player player = new Player("Federer", 3);
		//Act
		String scoreAsString = player.getScoreAsString();
		//Assert
		assertEquals("forty", scoreAsString);
	}
	
	//class <0
	@Test
	public void scoreShouldBeNullIfNegative() {
		//Arrange
		Player player = new Player("Federer", -1);
		//Act
		String scoreAsString = player.getScoreAsString();
		//Assert
		assertNull(scoreAsString);
	}
	
	//class >3
	@Test
	public void scoreShouldBeNullIfMoreThanThree() {
		//Arrange
		Player player = new Player("Federer", 4);
		//Act
		String scoreAsString = player.getScoreAsString();
		//Assert
		assertNull(scoreAsString);
	}
	
	//method isTieWith()
	//class pareggio
	@Test
	public void shouldBeTie() {
		//Arrange
		Player player1 = new Player("Federer", 2);
		Player player2 = new Player("Nadal", 2);
		//Act
		boolean isTie = player1.isTieWith(player2);
		//Assert
		assertTrue(isTie);
	}
	
	//class nonPareggio
	@Test
	public void shouldNotBeTie() {
		//Arrange
		Player player1 = new Player("Federer", 3);
		Player player2 = new Player("Nadal", 2);
		//Act
		boolean isTie = player1.isTieWith(player2);
		//Assert
		assertFalse(isTie);
	}
	
	//method hasAtLeastFortyPoints()
	//class has at least forty
	@Test
	public void shouldHaveAtLeastFortyPoints() {
		//Arrange
		Player player = new Player("Federer", 3);
		//Act
		boolean outcome = player.hasAtLeastFortyPoints();
		//Assert
		assertTrue(outcome);
	}
	
	//class not has at least forty
	@Test
	public void shouldNotHaveAtLeastFortyPoints() {
		//Arrange
		Player player = new Player("Federer", 2);
		//Act
		boolean outcome = player.hasAtLeastFortyPoints();
		//Assert
		assertFalse(outcome);
	}
	
	//method hasLessThanFortyPoints()
	//class
	@Test
	public void shouldHaveLessThenFortyPoints() {
		//Arrange
		Player player = new Player("Federer", 2);
		//Act
		boolean outcome = player.hasLessThanFortyPoints();
		//Assert
		assertTrue(outcome);
	}
	
	//class
	@Test
	public void shouldNotHaveLessThenFortyPoints() {
		//Arrange
		Player player = new Player("Federer", 3);
		//Act
		boolean outcome = player.hasLessThanFortyPoints();
		//Assert
		assertFalse(outcome);
	}
	
	//method hasMoreThanFourtyPoints()
	//class
	@Test
	public void shouldHaveMoreThenFortyPoints() {
		//Arrange
		Player player = new Player("Federer", 4);
		//Act
		boolean outcome = player.hasMoreThanFourtyPoints();
		//Assert
		assertTrue(outcome);
	}
	
	//class
	@Test
	public void shouldNotHaveMoreThenFortyPoints() {
		//Arrange
		Player player = new Player("Federer", 3);
		//Act
		boolean outcome = player.hasMoreThanFourtyPoints();
		//Assert
		assertFalse(outcome);
	}
	
	//method hasOnePointAdvantageOn()
	//class
	@Test
	public void shouldHaveOnePointAdvantageOn() {
		//Arrange
		Player player1 = new Player("Federer", 4);
		Player player2 = new Player("Nadal", 3);
		//Act
		boolean outcome = player1.hasOnePointAdvantageOn(player2);
		//Assert
		assertTrue(outcome);
	}
	
	//class
	@Test
	public void shouldNotHaveOnePointAdvantageOn() {
		//Arrange
		Player player1 = new Player("Federer", 3);
		Player player2 = new Player("Nadal", 3);
		//Act
		boolean outcome = player1.hasOnePointAdvantageOn(player2);
		//Assert
		assertFalse(outcome);
	}
	
	//method hasAtLeastTwoPointsAdvantageOn()
	//class
	@Test
	public void shouldHaveAtLeastTwoPointAdvantageOn() {
		//Arrange
		Player player1 = new Player("Federer", 4);
		Player player2 = new Player("Nadal", 2);
		//Act
		boolean outcome = player1.hasAtLeastTwoPointsAdvantageOn(player2);
		//Assert
		assertTrue(outcome);		
	}
		
	//class
	@Test
	public void shouldNotHaveAtLeastTwoPointAdvantageOn() {
		//Arrange
		Player player1 = new Player("Federer", 3);
		Player player2 = new Player("Nadal", 3);
		//Act
		boolean outcome = player1.hasAtLeastTwoPointsAdvantageOn(player2);
		//Assert
		assertFalse(outcome);
	}
}
